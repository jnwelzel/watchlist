'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  DOMAIN:           'http://127.0.0.1:9000',
  SESSION_SECRET:   'watchlist-secret',

  TWITTER_ID:       '',
  TWITTER_SECRET:   '',

  // Cloudinary credentials
  CLOUDINARY_NAME:    '',
  CLOUDINARY_KEY:     '',
  CLOUDINARY_SECRET:  '',
  // Control debug level for modules using visionmedia/debug
  DEBUG: ''
};
