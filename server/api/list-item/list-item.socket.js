/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ListItem = require('./list-item.model');

exports.register = function(socket) {
  ListItem.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ListItem.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('list-item:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('list-item:remove', doc);
}