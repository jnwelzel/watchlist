'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema

var ListItemSchema = new Schema({
  name: String,
  description: String,
  date: Date,
  media: String,
  poster: {type: String, default: 'assets/images/poster_default.png'},
  favorite: {type: Boolean, default: false}
});

module.exports = mongoose.model('ListItem', ListItemSchema);
