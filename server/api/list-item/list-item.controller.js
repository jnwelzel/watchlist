'use strict';

var _ = require('lodash');
var ListItem = require('./list-item.model');

// Get list of listItems
exports.index = function(req, res) {
  ListItem.find(function (err, listItems) {
    if(err) { return handleError(res, err); }
    return res.json(200, listItems);
  });
};

// Get a single listItem
exports.show = function(req, res) {
  ListItem.findById(req.params.id, function (err, listItem) {
    if(err) { return handleError(res, err); }
    if(!listItem) { return res.send(404); }
    return res.json(listItem);
  });
};

// Creates a new listItem in the DB.
exports.create = function(req, res) {
  ListItem.create(req.body, function(err, listItem) {
    if(err) { return handleError(res, err); }
    return res.json(201, listItem);
  });
};

// Updates an existing listItem in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ListItem.findById(req.params.id, function (err, listItem) {
    if (err) { return handleError(res, err); }
    if(!listItem) { return res.send(404); }
    var updated = _.merge(listItem, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, listItem);
    });
  });
};

// Deletes a listItem from the DB.
exports.destroy = function(req, res) {
  ListItem.findById(req.params.id, function (err, listItem) {
    if(err) { return handleError(res, err); }
    if(!listItem) { return res.send(404); }
    listItem.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
