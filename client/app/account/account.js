'use strict';

angular.module('watchlistApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupCtrl'
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        authenticate: true
      })
      .state('mylist', {
        url: '/my-list',
        templateUrl: 'app/account/mylist/mylist.html',
        controller: 'MyListCtrl',
        authenticate: true
      })
        .state('mylist.all', {
          url: '/all',
          templateUrl: 'app/account/mylist/mylist.all.html',
          controller: 'MyListAllCtrl',
          authenticate: true
        })
        .state('mylist.favorites', {
          url: '/favorites',
          templateUrl: 'app/account/mylist/mylist.favorites.html',
          controller: 'MyListFavoritesCtrl',
          authenticate: true
        })
        .state('mylist.upcoming', {
          url: '/upcoming',
          templateUrl: 'app/account/mylist/mylist.upcoming.html',
          controller: 'MyListUpcomingCtrl',
          authenticate: true
        })
        .state('mylist.tagged', {
          url: '/tagged',
          templateUrl: 'app/account/mylist/mylist.tagged.html',
          controller: 'MyListTaggedCtrl',
          authenticate: true
        });
});
