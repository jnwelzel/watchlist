'use strict';

angular.module('watchlistApp')
  .controller('NewShowModalCtrl', ['$scope', '$http', '$modalInstance', 'data', 'ngProgress', 'Auth', function ($scope, $http, $modalInstance, data, ngProgress, Auth) {

    $scope.save = function(show) {
      // console.log(angular.copy(show));
      // ngProgress.color('#2299DD');
      ngProgress.start();
      $http.post('/api/users/' + Auth.getCurrentUser()._id + '/pushShow', angular.copy(show))
        .success(function(newShow) {
          ngProgress.complete();
          $modalInstance.close(newShow);
        })
        .error(function() {
          ngProgress.complete();
        });
    };

    $scope.cancel = function() {
      $modalInstance.close(false);
    };

  }]);
