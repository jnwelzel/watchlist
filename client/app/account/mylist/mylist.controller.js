'use strict';

angular.module('watchlistApp')
  .controller('MyListCtrl', ['$scope', '$http', 'Auth', '$location', 'dialogs', function ($scope, $http, Auth, $location, dialogs) {

    $http.get('/api/users/me').success(function(data) {
      $scope.allShowsCount = data.shows.length;
      $scope.allFavoritesCount = data.shows.filter(function(element) {
        return element.favorite;
      }).length;
    });

    $scope.openModal = function() {
      var dialog = dialogs.create('app/account/mylist/newshowmodal.html', 'NewShowModalCtrl', null, {size: 'md'});
      dialog.result.then(function(newShow) {
        if (newShow) {
          $scope.$broadcast('addedNewShow', newShow);
        }
      });
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };

  }]);
