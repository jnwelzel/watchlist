'use strict';

describe('Controller: MylistCtrl', function () {

  // load the controller's module
  beforeEach(module('watchlistApp'));

  var MylistCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MylistCtrl = $controller('MylistCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
