'use strict';

angular.module('watchlistApp')
  .controller('MyListFavoritesCtrl', ['$scope', '$http', 'Auth', '$location', 'dialogs', function ($scope, $http, Auth, $location, dialogs) {

    $http.get('/api/users/me').success(function(data) {
      $scope.user = data;
      $scope.favorites = $scope.user.shows.filter(function(element) {
        return element.favorite;
      });
    });

  }]);
