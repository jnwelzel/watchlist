'use strict';

angular.module('watchlistApp')
  .controller('MyListAllCtrl', ['$scope', '$http', 'Auth', 'ngFoobar', function ($scope, $http, Auth, ngFoobar) {

    $http.get('/api/users/me').success(function(data) {
      $scope.user = data;
      $scope.items = $scope.user.shows;
    });

    $scope.$on('addedNewShow', function(event, show) {
      ngFoobar.show('success', 'New show successfully added');
      $scope.items.push(show);
    });

  }]);
